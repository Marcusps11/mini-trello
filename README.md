# Mini Trello

By Marcus Petty-Saphon


## Instructions

1. Navigate to https://bitbucket.org/Marcusps11/mini-trello
2. Clone locally using
   `git clone https://bitbucket.org/Marcusps11/mini-trello`
3. Install dependencies using `npm install`
4. Run using `npm run dev`
5. Navigate to app in [browser](http://localhost:8080)
6. Enjoy!


## Requirements

#### We’d like you to write a small Trello clone. At a minimum, the Trello clone should consist of:
### Lists and Cards.
1. A list has a required name and a collection of cards.
2. A card has a required title, required description and an optional due date.
A user of the application should:
1. Be able to create a new list.
2. Be able to create a new card in a list.
3. Be able to move cards between lists.
The application should:
1. Star cards that have a due date in the next 3 days.
2. Highlight cards in red that are overdue.
3. Persist lists and cards (however you like, but local storage is fine)

## Discussion

I used the following technologies: HTML, SCSS, React.
The app was completely from scratch using Webpack and Babel.

Given the time frame of the project I had to make certain decisions as to what to prioritise and how much of the task I managed to complete as you said to limit myself to 4 hours on the project.

With this project the user is able to create new list and also add cards to that list. Each card has an area where you add a required title and a required description.  I decided to leave the optional due date as it was related to the third part of the test which i planned to tacking if i had enough time.

I got as far as the stage of being able to move the cards between the lists part of the test.  Initally I tried to use the JS on drag API - however upon a little bit of investigation I realised I was probably going to have to use a ReactLibrary.
###React DnD
`http://react-dnd.github.io/react-dnd/`

Upon reading the documentation and playing around with it -  I saw that it was not the easiest thing to learn how to use and probably out of scope for the time that I had to complete the project.
Given more time this would be the next thing I would tackle followed by the date functionality.
