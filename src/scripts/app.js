import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import List from './components/list';
import Button from './components/button';
import InputField from './components/input-field';

class Trello extends Component {
  constructor(props) {
    super(props);
    this.state = {
        card: [],
        value: ''
    };
  }

  clickHandler = () => {
    if(this.state.value.length === 0 ) {
      alert('you need to add a list name')
      return false
    } else {
      this.setState({
        card: this.state.card.concat(<List key={this.state.card.length} title={this.state.value} />),
        value: ''
    });
    }
}

updateValueOfCard = (e) => {
  this.setState({ value: e.target.value });
}
  render() {

    return (
      <div className="trello">
        {this.state.card}
        <div className="trello__list-input-container">
          <Button handleClick={this.clickHandler} value="add list" />
          <InputField
          onChange={this.updateValueOfCard}
          value={this.state.value}
          placeHolder="Add list title"
          className="trello__list-title"
          />
        </div>
      </div>
    );
  }
}

 ReactDOM.render((
  <Trello />
), document.getElementById('root'))


