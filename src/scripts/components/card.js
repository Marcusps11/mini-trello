import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Description from './description';
import Button from './button.js';


class Card extends Component {
  constructor() {
    super()
    this.state = {
      saved: false,
      value: ''
    }
  }

  updateValueOfCard = (e) => {
    this.setState({ value: e.target.value });
  }

  handleClickEvent = () => {
    this.isItemSaved = false;
    this.setState({
      saved: true
    });
    this.isItemSaved = true;
  }

  getFullElememnt() {
    return <p>{this.state.value}</p>
  }

  render() {
    return (
      <div draggable="true" className="list__item" onDragStart={this.props.onDragStart}>
        <h4 className="list__item--title">{this.props.content}</h4>
        {this.isItemSaved = this.state.saved ? this.getFullElememnt() :
        <div>
          <Description onChange={this.updateValueOfCard} />
          <Button handleClick={this.handleClickEvent} value="Save Description" />
        </div>
        }
      </div>
    );
  }
}

export default Card;
