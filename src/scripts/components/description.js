import React, { Component } from 'react';
import ReactDOM from 'react-dom';

const Description = ({onChange, description}) =>
  <textarea type="text" rows="4" cols="10" placeholder="Add task description" onChange={onChange} >
    {description}
  </textarea>

export default Description;
