import React, { Component } from 'react';
import ReactDOM from 'react-dom';

const InputField = ({onChange, value, className, placeHolder}) =>
  <input required type="text" className={className} placeholder={placeHolder} onChange={onChange} value={value}  />

export default InputField;
