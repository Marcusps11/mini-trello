import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ListTitle from './list-title.js';
import InputField from './input-field.js';
import Button from './button.js';
import Card from './card.js'


class List extends Component {
  constructor() {
    super()
    this.state = {
      value: '',
      inputList: []
    }
  }

  handleClickEvent = () => {
    if(this.state.value.length === 0 ) {
      alert('you need to add a title')
      return false
    } else {
      const inputList = this.state.inputList;
      this.setState({
          inputList: inputList.concat(<Card key={inputList.length} content={this.state.value} onDragStart={this.dragStart} />),
          value: ''
      });
    }

  }

  updateValueOfCard = (e) => {
    this.setState({ value: e.target.value });
  }

  drop = (e) => {
    e.preventDefault();
    var data = e.dataTransfer.getData("text");
    console.log(e.currentTarget)

  }

  dragStart = (e) => {
    e.dataTransfer.setData("text", e.currentTarget);
    console.log(e.currentTarget)
  }

  preventDefault = (e) => {
    e.preventDefault();
  }

  render() {
    return (
      <div onDragOver={this.preventDefault} onDrop={this.drop} className="list">
        <ListTitle title={this.props.title} />
        {this.state.inputList}
        <InputField
         onChange={this.updateValueOfCard}
         value={this.state.value}
         placeHolder="Add task title"
         className="list__title"
        />
        <Button handleClick={this.handleClickEvent} value="save" />
    </div>
    );
  }
}

export default List;
